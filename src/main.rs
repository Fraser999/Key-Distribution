extern crate sodiumoxide;

use sodiumoxide::crypto::sign;

fn main() {
    let count = 1_000_000;
    let mut values = vec![Vec::with_capacity(count); 8];
    for _ in 0..count {
        let (public_key, _) = sign::gen_keypair();
        // println!("{:?}", public_key.0);
        // for (index, c) in public_key.0.iter().enumerate() {
        //     if index % 8 == 7 {
        //         print!("{:02x}   ", c);
        //     } else {
        //         print!("{:02x}", c);
        //     }
        // }
        // println!("");
        for i in 0..8 {
            let mut n = 0u32;
            for (index, c) in public_key.0[(i * 4)..((i + 1) * 4)].iter().enumerate() {
                let mut new_c = *c as u32;
                new_c = new_c << ((3 - index) * 8);
                n += new_c;
            }
            values[i].push(n);
        }
    }

    for mut vec in &mut values {
        vec.sort();
    }

    for x in 0..count {
        for vec in &values {
            print!("{:010} ", vec[x]);
        }
        println!("");
    }
}
